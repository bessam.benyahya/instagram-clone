// Import the functions you need from the SDKs you need
import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore, getFireStore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqeXYTZLpib0Z8pXinDYPMxo0BFqXrZYs",
  authDomain: "instagram-clone-b9a38.firebaseapp.com",
  projectId: "instagram-clone-b9a38",
  storageBucket: "instagram-clone-b9a38.appspot.com",
  messagingSenderId: "33222296078",
  appId: "1:33222296078:web:bb71ebc1d3394f857eb25f",
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const storage = getStorage();

export { app, db, storage };
